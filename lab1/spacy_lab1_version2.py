# encoding=utf8
import spacy
import re  # Para expresioones regulares
from sklearn.feature_extraction import DictVectorizer
import csv
import pandas
import nltk

EOL = '\n'


# Devuelve una lista con las oraciones de un texto
def get_sentences(text):
    sentences = []
    sentences = nltk.sent_tokenize(text, 'spanish')

    return sentences


def preprocess(corpus):
    # Transformar el texto a lowercase
    text = unicode(open(corpus).read().decode('utf8')).lower()

    # dejo solo un espacio
    text = re.sub(' +',' ', text)

    # Reemplazamos los numeros por NUMBER
    # pensandolo bien sacaria todos los numeros
    # text = re.sub(r"[0-9]+", "NUMBER", text)
    text = re.sub(r"[0-9]+", "", text)

    sentences = get_sentences(text)
    return sentences, text


def get_clean_words(doc):
    # sacamos la lista de todas las palabras a partir del documento de spacy
    # aprovechamos y limpiamos las stopwords
    clean_words = []
    no_deseadas = nltk.stopwords.words('spanish')
    for token in doc:
        if token not in no_deseadas:
            clean_words.append(token)

    return clean_words

def get_context(document):
    # Deberia descartar los signos de puntuacion. Pero no del contexto
    words = []
    word_context = []
    word_context_count = []
    no_deseada = [',', '.', ':', ';', '?', '¿',
        '"', "'", '-', '(', ')', '\n']
    for i in range(1, len(doc)-1):
        context = {}

        prevw = doc[i-1]
        word = doc[i]
        nextw = doc[i+1]

        if word.orth_ not in no_deseada:
            context['main'] = word.orth_
            # context['main_pos'] = word.pos_

            # Guardamos la misma info para nuestras palabras vecinas
            context['previous'] = prevw.orth_
            # context['previous_pos'] = prevw.pos_
            context['next'] = nextw.orth_
            # context['next_pos'] = nextw.pos_

            words.append(word.orth_)
            word_context.append(context)

    return words, word_context#, word_context_count


def to_disk(data, save_to):
    dataframe = pandas.DataFrame(data)
    dataframe.to_csv(save_to, index=False, header=False, encoding='utf-8')

    return 0

'''
        if word in word_dict:
            if context in word_dict[word]:
                word_dict[word][context]+=1
            else:
                word_dict[word][context]=1
        else:
            word_dict[word] = {context:1}
    print word_dict
'''

# pre procesamos el texto, limpiamos lo que no queremos
sentences, clean_text = preprocess('test.txt')

# Cargamos la libreria
nlp = spacy.load('es')

# aplicamos spacy al corpus
# doc[i] = iesima palabra tokenizada
# En doc.sents estan todas las oraciones.
# Para listar las oraciones: >>> list(doc.sents)

# Paralelizamos a partir de las sentences del texto
# for doc in nlp.pipe(sentences, batch_size=len(sentences), n_threads=3):
doc = nlp(clean_text)

# Obtenemos las palabras
clean_words = get_clean_words(doc)



# Extraemos los features: En mi caso los contextos
words, context_words = get_context(doc)
print words

# Guardo los datos en csv
to_disk(words, 'words.csv')
to_disk(context_words, 'context_words.csv')

# Vectorizamos el diccionario de contextos
v = DictVectorizer(sparse=False)
X = v.fit_transform(context_words)
Y = v.inverse_transform(X)

#

import nltk
import numpy as np
import re
from nltk.corpus import PlaintextCorpusReader
from nltk.tokenize import RegexpTokenizer
from nltk.stem import SnowballStemmer #Para lemmatizacion
from scipy.sparse import dok_matrix
from nltk.corpus import stopwords
from sklearn.cluster import KMeans

#Devuelve una lista de oraciones y lista de palabras
def tokenize(text, clean_stopwords):
    punctuation = re.compile(r'[-.?!,":;()|0-9]')
    pattern = r'''(?ix)    # set flag to allow verbose regexps
     (?:[A-Z]\.)+        # abbreviations, e.g. U.S.A.
   | \w+(?:-\w+)*        # words with optional internal hyphens
   | \$?\d+(?:\.\d+)?%?  # currency and percentages, e.g. $12.40, 82%
   | \.\.\.            # ellipsis
   | [][.,;"'?():-_`]  # these are separate tokens; includes ], [
    '''
    tokenizer = RegexpTokenizer(pattern)
    corpus = PlaintextCorpusReader('.', text, word_tokenizer=tokenizer)
    sents_corpus = corpus.sents()
    words_corpus = corpus.words()

    aux_sents = []
    for sent in sents_corpus:
        aux_sents.append([punctuation.sub("", word) for word in sent])
        aux_sents[len(aux_sents)-1] = list(filter(None, aux_sents[len(aux_sents)-1]))
    sents_corpus = aux_sents
    words_corpus = [punctuation.sub("", word) for word in words_corpus]
    words_corpus = list(filter(None, words_corpus))
    if clean_stopwords :
        stop = stopwords.words('spanish')
        aux_sents = []
        for sent in sents_corpus:
                aux_sents.append([w for w in sent if w not in stop])
        sents_corpus = aux_sents
        words_corpus = [w for w in words_corpus if w not in stop]
    return (sents_corpus, words_corpus)




#Construye la matriz de contextos
def matriz_construct(dicWords, sentences):
    stemmer = SnowballStemmer('spanish')
    len_dic = len(dicWords)
    m = dok_matrix((len_dic,len_dic))
    for sent in sentences:
        length_sent = len(sent)
        for i_word in range(0,length_sent-1):
            w = stemmer.stem( sent[i_word].lower())
            w_pos = stemmer.stem( sent[i_word+1].lower())
            m[dicWords[w], dicWords[w_pos]] +=1
    return(m)



def dic_construct(words):
    stemmer = SnowballStemmer('spanish')
    dicWords = {} #diccionario que tiene clave: palabra, value: index en la lista
    list_index = [] #Lista de indices
    for word in words:
        w = stemmer.stem( word.lower()) #Lematiza
        if dicWords.get(w, -1) < 0: #no esta en dic
            len_dic = len(dicWords)
            dicWords[w] = len_dic
            list_index.append(w)
    return(dicWords, list_index)

#inicializa todo lo necesario para el entrenamiento
def inicialize(text, clean_text):
    sents_corpus, word_corpus = tokenize(text, clean_text)
    dicWords, list_index = dic_construct(word_corpus)
    m = matriz_construct(dicWords, sents_corpus)
    return(sents_corpus, word_corpus,dicWords, list_index, m)

#Entrenamiento kmeans
def clusters(n_clusters, m):
   return (KMeans(n_clusters=n_clusters, random_state=0).fit(m))

#IMprime los clusters y las palabras asociadas a cada cluster
def print_clusters(clusters_n, labels, list_index):
    for i in range(clusters_n):
        print("-----------------------------")
        print("CLUSTER: " , i)
        for t in enumerate (labels):
            if(t[1] == i):
                print (list_index[t[0]])

#Para ejecutar:
# text = "lavoztextodump.txt"
# clen_stopwords = True (Setear en true si se quiere un corpus sin stopwords, False caso contrario)
# sents_corpus, word_corpus,dicWords, list_index, m = clustering.inicialize(text, clean_stopwords)
# n_clusters = 3 (setear cantidad de clusters que quieras)
# kmeans = clusters(n_clusters, m)
# labels = kmeans.labels_
# print_clusters(n_clusters, labels, list_index)

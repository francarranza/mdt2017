# encoding=utf8
import nltk
import os
import sys
import re
from nltk import word_tokenize, pos_tag


def print_dict_value(d, sort):
    d_view = [(v, k) for k, v in d.iteritems()]
    d_view.sort(reverse=sort)  # natively sort tuples by first element
    for v, k in d_view:
        print "%s: %d" % (k, v)


# Hay que setear para que reconozca algunos caracteres en español
reload(sys)
sys.setdefaultencoding('utf8')

corpus = 'corpus_lavoz.txt'
# corpus = 'test.txt'
text = unicode(open(corpus).read().decode('utf8')).lower()
clean_text = re.sub(r'[0-9]+', 'NUMBER', text)

# Tokenizamos con el parametro español en nltk
# Problema: a la palabra ¡Hola no la tokeniza en [¡, Hola] sino [¡Hola]
tokenized = word_tokenize(clean_text, 'spanish')
"""
# Hago la featurizacion
features = {}
for index, token in enumerate(tokenized):
    if token in features:
        features[token].append(index)
    else:
        features[token] = [index]
"""

# Hago el diccionario de palabras. word_dict
# Ya que estamos, hago otro dict con las palabras y sus ocurrencias. word_count_dict
word_dict = {}
word_count_dict = {}
count = 0
for word in tokenized:
    if not word in word_dict:
        count += 1
        word_dict[word] = count

    if word in word_count_dict:
        word_count_dict[word] += 1
    else:
        word_count_dict[word] = 1

# Hago los contextos. Elijo 3 palabras, donde mi palabra esta al medio
# Ya que estamos hago el diccionario de contextos con sus count
word_context = {}
context_dict = {}
for i in range(1, len(tokenized) - 1):
    prevw = word_dict[tokenized[i - 1]]
    currentw = word_dict[tokenized[i]]
    nextw = word_dict[tokenized[i + 1]]
    context = (prevw, currentw, nextw)

    if currentw in word_context:
        if context in word_context[currentw]:
            word_context[currentw][context] += 1
        else:
            word_context[currentw][context] = 1
    else:
        word_context[currentw] = {context: 1}

    if context in context_dict:
        context_dict[context] += 1
    else:
        context_dict[context] = 1

print_dict_value(context_dict, False)

# Diccionario de los contextos donde {palabra:
# Taggeamos

# Normalizamos

# Word2vec

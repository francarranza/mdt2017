# encoding=utf8
import re  # Para expresioones regulares
import nltk
import numpy
from sklearn.cluster import KMeans
from scipy.sparse import dok_matrix
import bisect
import itertools # para iterar un generator

import codecs
from pprint import pprint
from sklearn.feature_extraction import DictVectorizer
from sklearn.decomposition import TruncatedSVD
from sklearn.feature_selection import SelectKBest, chi2


EOL = '\n'
STOPWORDS = nltk.corpus.stopwords.words('spanish')


# Para imprimir un diccionario
def print_dict(lis):
    for d in lis:
        print("{" + "\n".join("{}: {}".format(k, v) for k, v in d.items()) + "}")
        print('')

# Forma muy eficiente de buscar un elemento en una lista ordenada
def find_index(vocab, w):
    i = bisect.bisect_left(vocab, w)
    if i != len(vocab) and vocab[i] == w:
        return i
    return False


def insert_in_position(vocab, w, position):
    if position == len(vocab) or vocab[position] != w:
        vocab.insert(position, w)


# Agrega un elemento en una lista ordenada
def insert_ordered(vocab, w):
    position = bisect.bisect_left(vocab, w)
    insert_in_position(vocab, w, position)

    return position


def is_nice_word(word):
    status = True

    if find_index(STOPWORDS, word) or len(word)<=2:
        status = False

    return status


def open_file(corpus):
    tokenized = []
    for filename in corpus:
        with codecs.open('corpus/'+filename, 'r', 'iso-8859-1') as f:
            lines = f.readlines()

        for line in lines:
            tup = line.split("\n")[0].split(' ')
            if not tup[0].startswith('<') and len(tup) == 4:
                if not tup[2].startswith('F') and not tup[0] == '' \
                    and is_nice_word(tup[1]) and not tup[0].startswith('ENDOFARTICLE') \
                    and not tup[0].startswith('[') and tup[0].isalpha():
                    tokenized.append(tup[0:3])

    return tokenized


def get_features(tokenized):
    features = []
    vocab = []
    pos_list = []

    for i in range(1, len(tokenized)-1):
        dic = {}
        if i%1000 == 0:
            print(i,'palabras')

        j = find_index(vocab, tokenized[i][1])
        if not j:
            # Mi ventana de contexto
            dic['anterior=' +tokenized[i-1][1]] = 1
            dic['siguiente='+tokenized[i+1][1]] = 1

            # Para el pos, quiero las 2 primeras letras nomás
            dic['anterior_pos=' +tokenized[i-1][2][0:2]] = 1
            dic['siguiente_pos='+tokenized[i+1][2][0:2]] = 1

            # Agregar el diccionario de features a la lista ordenada
            #j = insert_ordered(vocab, tokenized[i][1])
            #insert_in_position(features, dic, j)
            #insert_in_position(control_list, tokenized[i][2][0:2], j)
            vocab.append(tokenized[i][1])
            features.append(dic)
            pos_list.append(tokenized[i][2][0:2])
        else:
            try:
                features[j]['anterior='+tokenized[i-1][1]] += 1
            except KeyError:
                features[j]['anterior='+tokenized[i-1][1]] = 1

            try:
                features[j]['actual='+tokenized[i][1]] += 1
            except KeyError:
                features[j]['actual='+tokenized[i][1]] = 1

            try:
                features[j]['siguiente='+tokenized[i][1]] += 1
            except KeyError:
                features[j]['siguiente='+tokenized[i][1]] = 1

            try:
                features[j]['anterior_pos='+tokenized[i-1][2][0:2]] += 1
            except KeyError:
                features[j]['anterior_pos='+tokenized[i-1][2][0:2]] = 1

            try:
                features[j]['siguiente_pos='+tokenized[i+1][2][0:2]] += 1
            except KeyError:
                features[j]['siguiente_pos='+tokenized[i+1][2][0:2]] = 1

    return features, vocab, pos_list


def normalize(features):
    maximo = 0
    for dic in features:
        for key in dic:
            maximo = max(dic[key], maximo)

    for dic in features:
        for key in dic:
            dic[key] /= float(maximo)


def print_clusters(clusters_n, labels, vocab):
    for i in range(clusters_n):
        print("------------", "Cluster: ", i, "-----------")
        count = 0
        for j in range(len(labels)):
            try:
                if labels[j] == i:
                    print(vocab[j])
                    count += 1
                if count > 20:
                    print('...')
                    break
            except IndexError:
                pass

'''
corpus = ['spanishEtiquetado_10000_15000',
 'spanishEtiquetado_15000_20000',
 'spanishEtiquetado_110000_115000',
 'spanishEtiquetado_180000_185000',
 'spanishEtiquetado_120000_125000',
 'spanishEtiquetado_185000_190000']
'''


corpus = ['spanishEtiquetado_10000_15000',
]

#filename = 'corpus/test.txt'
tokenized = open_file(corpus)
print("Ya tokenizé")

features, vocab, pos_list = get_features(tokenized[0:100000])
print('Ya featuricé')

print(features[0:5])

normalize(features)
print('Ya normalicé')

print(features[0:5])

# Armamos los vectores a partir de los features. Nos queda una matriz.
v = DictVectorizer(sparse=True)
X = v.fit_transform(features)
print('Ya vectoricé')

# Hacemos feature selection
selected = SelectKBest(chi2, k=10).fit_transform(X, pos_list)

# Reducimos la dimensionalidad de la matriz
svd = TruncatedSVD(random_state=23)
svd.fit_transform(X)
print('Ya reduje la dimensionalidad')

# Ahora hacemos los clusters con KMeans
n_clusters = 7
n_jobs = -1
km = KMeans(n_clusters=n_clusters, n_jobs=n_jobs)
km.fit(X)
labels = km.predict(X)

#print_clusters(n_clusters, labels, vocab)

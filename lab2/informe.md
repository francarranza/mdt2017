Informe práctico 2: Feature Selection
=====================================

* Francisco Carranza
* francar94 at gmail.com

### 1. Tokenización
Utilicé el corpus provisto por la materia: El Wikicorpus en español. Para la
tarea se recorrió línea por línea del corpus y a la vez descartando palabras
no deseadas, como por ejemplo: Signos de puntuación, líneas vacías y otros
caracteres raros. 

Se creó una lista de 3-uplas con: Palabra, lemma y pos. Una muestra de la misma:
```python
[[u'Acontecimientos', u'acontecimientos', u'NP00000'],
 [u'Nacimientos', u'nacimientos', u'NP00000'],
 [u'Fallecimientos', u'fallecimientos', u'NP00000'],
 [u'santo', u'santo', u'NCMS000'],
 [u'espa\xf1ol', u'espa\xf1ol', u'AQ0MS0'],
 [u'Erquinoaldo', u'erquinoaldo', u'NP00000'],
 [u'mayordomo', u'mayordomo', u'NCMS000'],
 [u'franco', u'franco', u'AQ0MS0'],
 [u'palacio', u'palacio', u'NCMS000'],
 [u'Neustria', u'neustria', u'NP00000']]
```
 
### 2. Featurización
En esta etapa se optó por utilizar:

    * La palabra siguiente
    * La palabra anterior
    * El pos de la siguiente
    * El pos de la anterior

Cabe destacar que solo se utilizaron los lemmas de cada palabra.
Finalizando, me quedé con 3 listas:

    1. El vocabulario del corpus (sin palabras repetidas)
    2. Una lista de features para cada palabra.
    3. Un vector de los Pos de cada palabra del vocabulario 

### 3. Normalizamos, Vectorizamos 
Antes de armar las matrices, convendría normalizar los features. Como vimos en 
clases, ésto NO provoca pérdida de información. Lo que hacemos es llevar los
datos a un rango entre [0,1].

#### Ejemplo gráfico:
Antes de normalizar:
```python
{u'anterior_pos=NP': 1, 
u'siguiente=fallecimientos': 4, 
u'anterior=acontecimientos': 2, 
u'siguiente_pos=NP': 1}
```

Nos quedará:
```python
{u'anterior_pos=NP': 0.25, 
u'siguiente=fallecimientos': 1, 
u'anterior=acontecimientos': 0.5, 
u'siguiente_pos=NP': 0.25}
```
Luego usamos la función **DictVectorizer** de la librería scipy con un flag para
trabajar con sparce matrices y evitar la sobrecarga de memoria.


### Feature Selection no supervisado
Para hacer una reducción de dimensionalidad de la matriz esparsa, utilicé el 
algoritmo de **TruncatedSVD** de *sklearn*. Notar que el algoritmo descarta
dimensiones de la matriz, que es lo mismo que decir que descarta features.

### Feature Selection supervisado
Para este caso sí usaremos el tercer vector creado a la hora de featurizar.
Con el algoritmo **KBest** de la librería *sklearn*, hacemos el feature selection.
Le pasamos como parámetros la matriz esparsa y el vector objetivo (en este caso
el de pos) y un K que es la cantidad de features a conservar.


### 4. Clusterización
Utilizamos el algoritmo de **KMeans** de la librería sklearn con un objetivo desde
20 a 80 clústers. Notamos que un problema persiste, siempre hay un cluster mucho
mas denso que los otros. Pero en el resto, se los ve bastante parejos en cuanto 
a cantidad de palabras.


### 5. Resultados
Noté una mejor sustancial en cuanto a los sentidos de los clústers. Creo que
el hecho de que sean más uniformes ha contribuido a esto. Por último notar que
el enfoque de feature selection supervisado funciona mejor ya que contamos con
más información que el no-supervisado.

Yo creo que para seguir mejorando se podría seguir trabajando en los features.
En este nuevo corpus hay muchas palabras sin sentido, mal escritas o en otros
idiomas. Habrá que ver si haciendo esta limpieza sigue mejorando la semántica 
de los clústers.

# encoding=utf8
import re  # Para expresioones regulares
import nltk
import numpy
from sklearn.cluster import KMeans
from scipy.sparse import dok_matrix
import bisect
import itertools # para iterar un generator

import codecs
from pprint import pprint
from sklearn.feature_extraction import DictVectorizer
from sklearn.decomposition import TruncatedSVD


EOL = '\n'
STOPWORDS = nltk.corpus.stopwords.words('spanish')


# Para imprimir un diccionario
def print_dict(lis):
    for d in lis:
        print("{" + "\n".join("{}: {}".format(k, v) for k, v in d.items()) + "}")
        print('')

# Forma muy eficiente de buscar un elemento en una lista ordenada
def find_index(vocab, w):
    i = bisect.bisect_left(vocab, w)
    if i != len(vocab) and vocab[i] == w:
        return i
    return False


def insert_in_position(vocab, w, position):

    if position == len(vocab) or vocab[position] != w:
        vocab.insert(position, w)


# Agrega un elemento en una lista ordenada
def insert_ordered(vocab, w):
    position = bisect.bisect_left(vocab, w)
    insert_in_position(vocab, w, position)

    return position


class Tokenization():
    def __init__(self, corpus):
        self.tokenized = self.open_file(corpus)
        print("Ya tokenizé")

    def get_tokens(self):
        return self.tokenized

    def _is_nice_word(self, word):
        status = True

        if find_index(STOPWORDS, word) or len(word)<=2:
            status = False

        return status


    def open_file(self, corpus):
        tokenized = []
        for filename in corpus:
            with codecs.open('corpus/'+filename, 'r', 'iso-8859-1') as f:
                lines = f.readlines()

            for line in lines:
                tup = line.split("\n")[0].split(' ')
                if not tup[0].startswith('<') and len(tup) == 4:
                    if not tup[2].startswith('F') and not tup[0] == '' \
                        and self._is_nice_word(tup[1]) and not tup[0].startswith('ENDOFARTICLE') \
                        and not tup[0].startswith('[') and tup[0].isalpha():
                        tokenized.append(tup[0:3])

        return tokenized


class Featurization():
    def __init__(self, tokenized):
        self.vocab, self.features, self.pos_list = self.init(tokenized)
        print('Ya featuricé')

    def get_features(self):
        return self.features

    def get_vocab(self):
        return self.vocab

    def get_pos(self):
        return self.pos_list


    def init(self, tokenized):
        features = []
        vocab = []
        pos_list = []

        for i in range(1, len(tokenized)-1):
            dic = {}
            if i%1000 == 0:
                print(i,'palabras')

            j = find_index(vocab, tokenized[i][1])
            if not j:
                # Mi ventana de contexto
                dic['anterior=' +tokenized[i-1][1]] = 1
                dic['siguiente='+tokenized[i+1][1]] = 1

                # Para el pos, quiero las 2 primeras letras nomás
                dic['anterior_pos=' +tokenized[i-1][2][0:2]] = 1
                dic['siguiente_pos='+tokenized[i+1][2][0:2]] = 1

                # Agregar el diccionario de features a la lista ordenada
                j = insert_ordered(vocab, tokenized[i][1])
                insert_in_position(features, dic, j)
                #insert_in_position(pos_list, tokenized[i][2][0:2], j)
            else:
                try:
                    features[j]['anterior='+tokenized[i-1][1]] += 1
                except KeyError:
                    features[j]['anterior='+tokenized[i-1][1]] = 1

                try:
                    features[j]['siguiente='+tokenized[i][1]] += 1
                except KeyError:
                    features[j]['siguiente='+tokenized[i][1]] = 1

                try:
                    features[j]['anterior_pos='+tokenized[i-1][2][0:2]] += 1
                except KeyError:
                    features[j]['anterior_pos='+tokenized[i-1][2][0:2]] = 1

                try:
                    features[j]['siguiente_pos='+tokenized[i+1][2][0:2]] += 1
                except KeyError:
                    features[j]['siguiente_pos='+tokenized[i+1][2][0:2]] = 1

        return features, vocab, pos_list


class Normalization():
    def __init__(self, features):
        self.features = self.normalize(features)
        print('Ya normalicé')

    def get_normalized():
        return self.features

    def normalize(self, features):
        maximo = 0
        for dic in features:
            for key in dic:
                maximo = max(dic[key], maximo)

        for dic in features:
            for key in dic:
                dic[key] /= float(maximo)

        return features


class Vectorizer():
    def __init__(self, features):
        self.vectors = self._vectorize(features)
        self.reduced = self._reduce(self.vectors)

    def get_dictvectors(self):
        return self.reduced

    def _vectorize(self, features):
        # Armamos los vectores a partir de los features. Nos queda una matriz.
        v = DictVectorizer(sparse=True)
        X = v.fit_transform(features)
        print('Ya vectoricé')
        return X

    def _reduce(self, matrix):
        # Reducimos la dimensionalidad de la matriz
        svd = TruncatedSVD(random_state=23)
        svd.fit_transform(X)
        print('Ya reduje la dimensionalidad')


class Clustering():
    def __init__(self, matrix, n):
        self.clusters = _clusterize(matrix, n)
        print_clusters(n_clusters, labels, vocab)

    def get_clusters(self):
        return self.clusters

    def print_clusters(clusters_n, labels, vocab):
        for i in range(clusters_n):
            print("------------", "Cluser: ", i, "-----------")
            count = 0
            for j in range(len(labels)):
                if labels[j] == i:
                    print(vocab[j])
                    count += 1
                if count > 20:
                    print('...')
                    break

    def _clusterize(self, matrix, n):
        km = KMeans(n_clusters=n, n_jobs=-1)
        km.fit(matrix)
        labels = km.predict(matrix)

        return labels


class Run():
    def __init__(self):
        self.corpus = ['test.txt']
        self.tokenized = Tokenization(self.corpus).get_tokens()[0:1000]

        Features        = Featurization(self.tokenized)
        self.features   = Features.get_features()
        self.vocab      = Features.get_vocab()
        self.pos_list   = Features.get_pos()

        self.normalized = Normalization(self.features).get_normalized()
        self.dictvectors = Vectorization(self.normalized).get_dictvectors()
        self.clusters   = Clusterization(self.dictvectors, 30).get_clusters()

test = Run()

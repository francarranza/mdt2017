### Server Mini FaMAF
* user: adc201666@mini.famaf.unc.edu.ar
* pass: Roghoyaex5Ru

### Notas del primer lab
+ Posible evaluacion por lema
+ Caraduría del dataset y su relación (o no) con feature selection
+ algoritmo para convertir un corpus a vectores
+ Quedarse con las palabras que ocurren mas de 100, 200 veces. Características más de 50 veces. Luego, ir reduciendo el umbral.
+ Usar Word Embeddings en vez de triplas de dependencias

### Notas para el proyecto final

#### Ideas de proyectos
* Descubrimiento y construccion de ontologías (dbpedia por ejemplo)
* Sentiment analysis -> detection of opinionated? text with
transfer learning
* Remodelacion con ejemplos etiquetados y no etiquetados
* Capacidades de identity recognition en textos legales combinando 
clasificadores, datos no supervisados y heruísticas. Con eso hacer un
ensamble
+ Wikificar sentencias (de la corte). No tiene interés científico, pero tiene
un interés práctico muy grande. Técnicas de active learning.
+ end-to-end (?). 
+ Relación entre características y clases + PCA
(feature selection con corpus anotados muy chicos)
+ Ensembles para argument mining
+ Active learning para argument mining

No relacionados con ArgMine/Legal
* topic detection
* summarization
* (mi idea) Descubir de que me habla una cancion. Topic detection? Classification?
* Extracción de Información de informes clínicos
* Simplificación textual (Es un problema muy conocido. Para gente que no quiere
correr riesgos)
* Tareas del SemEval.
* El de los emojis.

#### Muro
Lo que está de moda en las redes convolucionales son los ensambles.
Las diferentes partes dicen algo sobre muchas cosas. Sirve para tratar cosas
muy complejas.

#### Como encarar un proyecto
1 Prospección, líneas generales. Definir bien el problema.
2 Buscar recursos: Datos y herramientas
3 Plan: Desarrollo, Resultados y Evaluación

Cada proyecto llevará 4 reuniones de media hora con Laura.
Se pueden compartir corpus

#### Mi tema: Topic detection in Song Lyrics
Helena y otra persona está en algo parecido. Meter si o si algo no supervisado
onda, hacer clustering para que sea más interesante.

Referencias
* OH OH OH WHOAH! Towards automatic topic detection in song lyrics.

### Practico 2:
Lista de oraciones dividida en palabras. Armar 2 diccionarios para usar el dict
vectorizer. Recibe un dicc de features para cada palabra.
Supongamos que tengo 100 palabras, 
al dictvectorizer le tengo que pasar un dict con 100 elementos donde cada elemento
es un diccionario de los features de esa palabra. Me arma solo la matriz.

#### Para los features:
* Palabras con pos

#### 3 Estrucutras
* Lista de todas las palabras con un índice.
* Lista que en cada indice tenés cada palabra.
* Diccionario de features






